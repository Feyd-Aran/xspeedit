# README #

Implémentation du use case xspeedit

##Execution du script##

Python 2.7

python xspeedit.py \[liste_articles]

##Implémentation basique##

On remplit au maximum les cartons avant de passer au suivant et ce dans l'ordre d'arrivée des cartons.

Algorithme de base:

Tant qu'il y a des articles à traiter
	Tant que carton pas plein:
		parcours des articles
			Si on peut mettre l'article dans le carton, on le met
			Sinon on passe à l'article suivant

Limitation:

Si les articles arrivent dans un ordre du plus léger au plus lourd, l'algorithme va remplir au maximum les premiers cartons
et ne va pas optimiser l'espace en utilisant les objets les plus lourds

Ex: sur une liste d'articles 123456789, on va obtenir 1234/5/6/7/8/9 soit 6 cartons au lieu d'un 5 optimisé

##Implémentation améliorée##

Si la possibilité est donnée, un tri des articles du plus lourd au plus léger permet d'optimiser le stockage des objets

##Commentaires et remarques##

* Le poids des articles est géré en numérique de 1 à 9. Tout poids non numérique lève une exception de type WeightException
* Si une chaine est passée en paramètre au script, cette chaine est utilisée comme articles à traiter sinon une chaine de taille MAX_ARTICLES est créée aléatoirement

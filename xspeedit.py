#! /usr/bin/python
#-*- coding:UTF8 -*-

import sys, random

'''Définition de constantes'''
MAX_ARTICLES = 10
MAX_WEIGHT = 10

class WeightException(Exception):
	'''Exception custom de vérification du poids. On accepte que les chiffres de 1 à 9'''
	pass

def valid_weight(weight):
	'''Vérification de la validité du poids d'un article sinon on lève une exception'''
	return weight < 1 or weight > 9

def compute_weight(carton):
	'''
	Retourne le poids d'un carton
	'''
	weight = 0
	for elt in carton:
		weight += int(elt)
	return weight

def process_articles(articles):
	
	cartons = ['']
	'''On vérifie que les articles ont tous un poids valide sinon on lève une exception'''
	for article in articles:
		if not valid_weight(article):
			raise WeightException('Poids invalide : '+str(article))

	'''Tant qu'il y a des articles à traiter'''
	while(len(articles)):
		'''On met le premier article dans le carton et on le dépile'''
		cartons[len(cartons)-1] = str(articles[0])
		articles = articles[1:]

		'''Parcours des articles restants pour compléter le carton'''
		i = 0
		while(i < len(articles)):
			'''Si l'objet ajouté au carton ne le fait pas dépasser la limite, on l'ajoute'''
			if(compute_weight(cartons[len(cartons)-1]) + int(articles[i]) <= MAX_WEIGHT):
				cartons[len(cartons)-1] += articles[i]
				'''Suppression de l'article ajouté de la chaine et recréation de la chaine à traiter'''
				articles = articles[:i]+articles[i+1:]
			else:
				'''Article suivant'''
				i += 1

		'''S'il reste des articles, on ajoute un carton'''
		if(len(articles)):
			cartons.append('')

	return cartons

if __name__ == "__main__":
	articles = ''

	'''Si on passe un argument au script, on utilise les articles passés en argument'''
	if len(sys.argv) == 2:
		articles = sys.argv[1]
	else:
		'''Sinon Création de la chaine d'articles à traiter et affichage'''
		while(len(articles) < MAX_ARTICLES):
			articles += str(random.randint(1,9))
	print(articles)

	'''Création de la liste des cartons'''
	cartons = process_articles(articles)

	'''Impression du résultat'''
	print('/'.join(cartons))
